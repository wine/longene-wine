/*
 * handle.h
 *
 * Copyright (C) 2006  Insigme Co., Ltd
 *
 * This software has been developed while working on the Linux Unified Kernel
 * project (http://linux.insigma.com.cn) in the Insigma Research Institute,  
 * which is a subdivision of Insigma Co., Ltd (http://www.insigma.com.cn).
 * 
 * The project is sponsored by Insigma Co., Ltd.
 *
 * The authors can be reached at linux@insigma.com.cn.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of  the GNU General  Public License as published by the
 * Free Software Foundation; either version 2 of the  License, or (at your
 * option) any later version.
 *
 * Revision History:
 *   Jan 2006 - Created.
 */
 
/*
 * handle.h: We use Unified Kernel handles to make it compatible with Wine's 
 *           operations.
 */

#ifndef __UK_HANDLE_H
#define __UK_HANDLE_H

#ifdef UNIFIED_KERNEL
#include "wine/list.h"

/* 
 * if the handle value greater than (1<<15), it will fault when someone 
 * call it
 */
#define UK_HANDLE_FLAG (1 << 15)
#define HANDLE_TO_UK_HANDLE(handle) \
    (HANDLE)((ULONG_PTR)(handle) | UK_HANDLE_FLAG)
#define UK_HANDLE_TO_HANDLE(handle)             \
    (HANDLE)((ULONG_PTR)(handle) & ~UK_HANDLE_FLAG)

#define IS_UK_HANDLE(handle) \
    ((ULONG_PTR)(handle) & UK_HANDLE_FLAG)

struct handle_pair
{
    struct list entry;
    HANDLE uk_handle;
    HANDLE wine_handle;
};

struct handle_pair* search_handle_pair(HANDLE uk_handle);
void delete_handle_pair(struct handle_pair* pair);
int store_handle_pair(HANDLE uk_handle, HANDLE wine_handle);
#endif /* UNIFIED_KERNEL */

#endif /* __UK_HANDLE_H */
